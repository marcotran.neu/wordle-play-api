import logging.config
import yaml
import os

# Get the directory of the current script
current_dir = os.path.dirname(os.path.abspath(__file__))

# Construct the path to the logging configuration file relative to the script's location
config_file_path = os.path.join(current_dir, '../config_folder/logging_config.yaml')


with open(config_file_path, 'r') as config_file:
    config = yaml.safe_load(config_file)

# Apply the configuration to the logging system
logging.config.dictConfig(config)
logger = logging.getLogger(__name__)
import logging.config
import yaml
import random
from nltk.corpus import words
from .logger import logger
import nltk

nltk.download('words')


class WordleSolver:
    def __init__(self):
        self.english_words = words.words()

    def generate_random_english_word(self, size: int):
        """
        The function generates a random English word of a specified size.
        
        :param size: The "size" parameter in the "generate_random_english_word" function is an integer
        that specifies the desired length of the English word to be generated
        :type size: int
        :return: a randomly chosen English word from a list of valid words that have the specified size.
        """
        valid_words = [word for word in self.english_words if len(word) == size]
        if valid_words:
            return random.choice(valid_words)
        else:
            logger.warning(f"No English words found with the specified size {size}")
            raise ValueError(f"No English words found with the specified size {size}")

    def get_feedback_check_word(self, secret_word: str, guess: str):
        """
        The function `get_feedback_check_word` takes a secret word and a guess as input and returns feedback
        on each letter in the guess, indicating whether it is correct, present in the secret word, or absent
        from the secret word.
        
        :param secret_word: The `secret_word` parameter is a string that represents the word that the player
        is trying to guess
        :type secret_word: str
        :param guess: The `guess` parameter is a string representing the player's guess for the secret word
        :type guess: str
        :return: a list of dictionaries. Each dictionary contains information about a letter in the guess.
        The dictionary includes the slot number, the guessed letter, and the result of the guess (either
        "correct", "present", or "absent").
        """
        feedback = []
        for i, letter in enumerate(guess):
            if letter == secret_word[i]:
                feedback.append({"slot": i, "guess": letter, "result": "correct"})
            elif letter in secret_word:
                feedback.append({"slot": i, "guess": letter, "result": "present"})
            else:
                feedback.append({"slot": i, "guess": letter, "result": "absent"})
        return feedback

    def check_word(self, guess: str):
        """
        The function checks if a given word is in a list of English words.
        
        :param guess: The parameter `guess` is a string that represents the word that needs to be
        checked
        :type guess: str
        :return: True if the guess is in the list of English words, and False otherwise.
        """
        return True if guess in self.english_words else False

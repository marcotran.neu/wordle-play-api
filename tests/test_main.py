import unittest
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


class TestWordsegEndpoint(unittest.TestCase):

    def test_wordseg(self):
        response = client.post("/wordseg", data={"text": "testword"})
        self.assertEqual(response.status_code, 200)
        self.assertIn("segments", response.json())
        self.assertEqual(response.json()["segments"], ["t", "te", "test", "word"])


class TestRandomGuessEndpoint(unittest.TestCase):

    def test_random_guess_missing_guess_parameter(self):
        response = client.get("/random?seed=123&size=5")
        self.assertEqual(response.status_code, 422)
        self.assertEqual(
            response.json(),
            {"detail": [{"loc": ["query", "guess"], "msg": "field required", "type": "value_error.missing"}]}
        )

    def test_random_guess_invalid_guess(self):
        response = client.get("/random?guess=xyz&seed=123&size=5")
        self.assertEqual(response.status_code, 422)
        self.assertEqual(
            response.json(),
            {"detail": [{"loc": ["query", "guess"], "msg": "Word not found", "type": "value_error.error"}]}
        )

    def test_random_guess_valid_guess(self):
        response = client.get("/random?guess=apple&seed=123&size=5")
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()

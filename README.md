# wordle play api

## Project structure
```text
.
|-- README.md
|-- app.log
|-- config_folder
|   `-- logging_config.yaml
|-- main.py
|-- requirements.txt
|-- tests
|   `-- test_main.py
`-- utils
    |-- logger.py
    |-- wordle_solver.py
    `-- wordseg.py
    
```
## Dependencies
```text
- python > 3.6
```
## How to run
```text
- pip install -r requirements.txt
- Run app
    Option 1: python main.py
    Option 2: uvicorn main:app --reload 
```
- Check http://localhost:8000
- API docs: http://localhost:8000/docs
- Random guess exam: http://localhost:8000/random?guess=apple&seed=123&size=5

## run the test
```text
 python -m unittest tests/test_main.py
```

## Idea
- Get word list from NLTK
- Get a word from list
- Compare the guess and the word


## Credit for
- ChatGPT: which give me a based for start
- Mintlify Doc Writer: Help me for documentation the code
- Github: https://github.com/marslansarwar171/wordle-game, https://github.com/Kinkelin/WordleCompetition
 
### NOTE
- This solution provide a bit simple and basic.
- For the future if do it again I will provide more than like implement (Redis, Celery) for more scale up
- Add more option like limit of guess, word finding.

